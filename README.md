A Little Info About Paul Duffy.

Paul grew up in Woburn, MA where he is one of six kids. 
He played football in High School. He also coached Pop Warner for eight years.

After high school Paul attended the University of MA where he played Rugby and graduated with honors.

He fought in the Golden Gloves but ended up losing to the two time champ.

After college he and three friends moved to San Diego specifically Pacific Beach. Fun times!

Paul left San Diego after 3 years and moved to Phoenix to pursue a career as a pilot. He completed his private and instrument rating and was working towards his commerical and multi licenses BUT he ran out of money. :-(.
He was going to get back into flying but due to 9/11 the airlines industry went through layoffs which ended Paul's career aspirations as a pilot.

Hello Technology Sales!
Paul has worked within the technology services arena for many years working with companies such as GE, McKesson, Siemens, Cerner and many more. 
Prior to Gitlab Paul worked for BMC Software.

Personal life

Paul has a three year old son named Evan and an eight year old daughter named Sarah. :-)
He enjoys spending time with his kids, excercising, traveling and cheering on The New England Patriots. When he gets the chance he also loves to ski. 
Charities Paul Supports: Make a Wish, St. Jude, National MS Society, Bulding Homes for Vets, Tanner Ta-Ta's.
